# Scripting with Java

* Can use jshell to test out different stuff. Run `jshell`. Output:

```
|  Welcome to JShell -- Version 12.0.2
|  For an introduction type: /help intro

jshell> 
```

* HelloWorld and output
```
jshell> System.out.println("HelloWord");
HelloWord

jshell> 
```

* Simulate simple use case of unix `ls` function. File can be excuted: `./ls /tmp ./ /`

