## Hello Gradle

1. Create java application with gradle by run `gradle init`. Select the option by typing the number just like in the following picture. 

![gradle-init](gradle-init.png)

2. Now you can run the program by `./gradlew run`. Output:

```
> Task :run
Hello world.
```
