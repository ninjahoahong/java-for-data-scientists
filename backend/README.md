# Simple server with vertx

This is a demo for a simple server to be created with vertix. This can be used to write api to query a trained model.

* Run `./gradlew run`. 
* `curl localhost:8080`. Output:

```
Hello from Vert.x!
```